//
//  ViewModel.h
//  ConvertArabicToRomanNumber
//
//  Created by Nikolay Chaban on 7/1/16.
//  Copyright © 2016 Nikolay Chaban. All rights reserved.
//

// Frameworks
#import <Foundation/Foundation.h>
#import "ReactiveCocoa.h"

@interface ViewModel : NSObject

// properties

@property (strong, nonatomic) NSString* arabicNumberString;

@property (strong, nonatomic, readonly) NSString* resultRomanNumber;

// methods



@end
