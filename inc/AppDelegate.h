//
//  AppDelegate.h
//  ConvertArabicToRomanNumber
//
//  Created by Nikolay Chaban on 7/1/16.
//  Copyright © 2016 Nikolay Chaban. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

