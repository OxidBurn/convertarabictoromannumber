//
//  ViewModel.m
//  ConvertArabicToRomanNumber
//
//  Created by Nikolay Chaban on 7/1/16.
//  Copyright © 2016 Nikolay Chaban. All rights reserved.
//

#import "ViewModel.h"

@interface ViewModel()

// properties

@property (strong, nonatomic) NSString* resultRomanNumber;

@property (strong, nonatomic) NSArray* romanStringValues;

@property (strong, nonatomic) NSArray* romanNumeralNumbers;

// methods

- (void) setupBindings;

@end

@implementation ViewModel

#pragma mark - Initialization -

- (instancetype)init
{
    if ( self = [super init] )
    {
        [self setupBindings];
    }
    
    return self;
}

#pragma mark - Properties -

- (NSArray*) romanStringValues
{
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        
        _romanStringValues = @[@"M", @"CM", @"D", @"CD", @"C", @"XC", @"L", @"XL", @"X", @"IX", @"V", @"IV", @"I"];
        
    });
    
    return _romanStringValues;
}

- (NSArray*) romanNumeralNumbers
{
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        
        _romanNumeralNumbers = @[@(1000), @(900), @(500), @(400), @(100), @(90), @(50), @(40), @(10), @(9), @(5), @(4), @(1)];
        
    });
    
    return _romanNumeralNumbers;
}

#pragma mark - Methods -

- (void) setupBindings
{
    @weakify(self)
    
    RAC(self, resultRomanNumber) = [RACObserve(self, arabicNumberString) map: ^NSString*(NSString* value) {
        
        @strongify(self)
        
        NSUInteger numberValue = value.integerValue;
        
        if ( value.length == 0 )
            return @"";
        else
            return [self convertArabicValueToRoman: numberValue];
        
    }];
}

- (NSString*) convertArabicValueToRoman: (NSUInteger) number
{
    if (number <= 0u || number > 4000)
    {
        return @"number value is not valid";
    }
    
    __block NSUInteger arabicNumber = number;
    
    __block NSMutableArray* result = [NSMutableArray array];
    
    [self.romanNumeralNumbers enumerateObjectsUsingBlock: ^(NSString* romanNumber, NSUInteger idx, BOOL * _Nonnull stop) {
        
        NSUInteger romanNumeralInteger = romanNumber.integerValue;
        
        while (arabicNumber >= romanNumeralInteger)
        {
            NSString* romanNumeralValue = self.romanStringValues[idx];
            
            [result addObject: romanNumeralValue];
            
            arabicNumber -= romanNumeralInteger;
        }
        
    }];
    
    return [result componentsJoinedByString: @""];
}

@end
