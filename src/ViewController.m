//
//  ViewController.m
//  ConvertArabicToRomanNumber
//
//  Created by Nikolay Chaban on 7/1/16.
//  Copyright © 2016 Nikolay Chaban. All rights reserved.
//

// Frameworks
#import "ReactiveCocoa.h"

// Classes
#import "ViewController.h"
#import "ViewModel.h"

@interface ViewController ()

// properties
@property (weak, nonatomic) IBOutlet UITextField* enteredField;

@property (weak, nonatomic) IBOutlet UILabel* resultLabel;

@property (strong, nonatomic) ViewModel* viewModel;

// methods

- (void) bindingUI;

@end

@implementation ViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self bindingUI];
}

#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Properties -

- (ViewModel*) viewModel
{
    if ( _viewModel == nil )
    {
        _viewModel = [[ViewModel alloc] init];
    }
    
    return _viewModel;
}

#pragma mark - Methods -

- (void) bindingUI
{
    RAC(self.viewModel, arabicNumberString) = [self.enteredField.rac_textSignal distinctUntilChanged];
    
    RAC(self.resultLabel, text) = RACObserve(self.viewModel, resultRomanNumber);
}

@end
